from time import sleep

class User:
    def __init__(self, email, password):
        self.email = email
        self.password = password
        self.logged_in = False

    def login(self, password):
        sleep(2)
        if password == self.password:
            self.logged_in = True

    def logout(self):
        sleep(1)
        self.logged_in = False
