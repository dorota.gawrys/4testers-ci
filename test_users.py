from users import User


def test_user_initial_state():
    test_user= User("test@example.com", "Password")
    assert test_user.email == "test@example.com"
    assert test_user.password == "Password"
    assert not test_user.logged_in


def test_user_logged():
    test_user = User("test@example.com", "Password")
    test_user.login("Password")
    assert test_user.logged_in

def test_user_not_logged():
    test_user = User("test@example.com", "Password")
    test_user.login("Wrongpassword")
    assert not test_user.logged_in


def test_logout():
    test_user = User("test@example.com", "Password")
    test_user.login("Password")
    test_user.logout()
    assert not test_user.logged_in











